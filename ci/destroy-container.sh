set -axe
CONTAINER_NAME=$1
echo "Destroying container $CONTAINER_NAME..."
docker stop "$CONTAINER_NAME"
