set -axe
CONTAINER_NAME=$1
DOMAIN_NAME=$2
PORT=$(docker inspect --format='{{range $p, $conf := .NetworkSettings.Ports}}{{(index $conf 0).HostPort}} {{end}}' $CONTAINER_NAME)
NGINX_CONFIG="server {\nserver_name $CONTAINER_NAME.$DOMAIN_NAME;\nlisten 80;\nlocation / { proxy_pass http://127.0.0.1:$PORT; }\n}"
echo $NGINX_CONFIG > /etc/nginx/conf.d/$CONTAINER_NAME.conf
nginx -s reload